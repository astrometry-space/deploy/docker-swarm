# Docker Swarm

Get started on Docker Swarm with one or more nodes

See [swarm_boot.sh](./swarm_boot.sh).  It expalains everything.

You should be able to do this:
```bash
./swarm_boot.sh
```

* [docker-compose.yml](./docker-compose.yml): contains the actual API and website

* [support.yml](./support.yml): contains supporting services (elastic, S3) that you can run locally

* [ec2.yml](./ec2.yml): contains ec2-specific overrides of `docker-compose.yml`


Example of deploying the EC2 override:
```bash
docker stack deploy -c docker-compose.yml -c ec2.yml astrometry
```