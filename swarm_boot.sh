#! /bin/bash

divider() {
    printf "\n-- -- -- -- -- -- -- -- -- --\n"
}

compute_image="registry.gitlab.com/astrometry-space/compute:latest"
stack_name="astrometry"
external_network=0 # set to "1" to create and external, attachable network

divider
echo "log into the gitlab.com docker registry:"
docker login registry.gitlab.com

# user must provide registry credentials if they want all nodes to participate in computations:
divider
if [[ -z $ASTROMETRY_DEPLOY_USER ]];
then
    echo "ASTROMETRY_DEPLOY_USER not set!  Other swarm nodes may not be able to pull images!"
fi
if [[ -z $ASTROMETRY_DEPLOY_TOKEN ]];
then
    echo "ASTROMETRY_DEPLOY_TOKEN not set!  Other swarm nodes may not be able to pull images!"
fi

divider
# Make sure we have a swarm:
if [[ $(docker node ls) ]];
then
    echo "This swarm is already initialized!"
else
    echo "This not a swarm. Initializing one:"
    docker swarm init
fi

# remove the stack if it already exists:
divider
docker stack rm ${stack_name}

divider
# check for the astrometry index volume:
volume_name="astrometry_index"
found_volume=$(docker volume ls --filter name=${volume_name} -q | wc -l)
if [[ $(($found_volume)) == 1 ]];
then
    echo "Found ${volume_name} docker volume:"
    docker volume inspect ${volume_name}
else
    echo "Did not find ${volume_name} docker volume!"
    echo "Creating volume now..."
    docker volume create ${volume_name}
    echo "Done:"
    docker volume inspect ${volume_name}
fi

divider
# check the size of the index volume:
minimum_index_size_mb=339
index_size_mb=$(docker run -it --rm -v ${volume_name}:/data alpine du -m /data | cut -f1)
echo "Volume ${volume_name} is uses ${index_size_mb}MB of disk"
if [[ ${index_size_mb} -lt ${minimum_index_size_mb} ]];
then
    echo "This seems small.  The 4100 catalogs usually take ~${minimum_index_size_mb}MB."
    read -p "Do you want to download 4100 now? (y/n)" -n 1 -r
    echo
    if [[ ${REPLY} =~ ^[Yy]$ ]];
    then
        echo "Downloading 4100 index..."
        docker run -it --rm -v ${volume_name}:/data --workdir /data --entrypoint bash ${compute_image} -c 'wget -r -np -nd -A fits "http://data.astrometry.net/4100/"'
        echo "Done downloading 4100 index!"
    else
        echo "OK. Just remember, the indicies are required in order solve fields!"
    fi
else
    echo "Index volume seems like it's the right size."
fi

divider
if [[ ${external_network} == "1" ]];
then
    echo "Using external network..."
    # make sure the network is there:
    network_name=astrometry
    if [[ $(docker network inspect ${network_name}) ]];
    then
        read -p "Network ${network_name} already exists. This will re-create it.  Continue? (y/n)" -n 1 -r
        if [[ ${REPLY} =~ ^[Yy]$ ]];
        then
            echo "Deleting existing network..."
            docker network rm ${network_name}
            echo "Done."
        fi
    fi
    echo "Creating overlay network: ${network_name}"
    docker network create -d overlay --attachable ${network_name}
    echo "Done."
else
    echo "Using internal stack network."
fi

divider
# now the easy part: deploy the services:
echo "Deploying the astrometry stack!"
docker stack deploy --with-registry-auth -c support.yml -c docker-compose.yml ${stack_name}

divider
# wait for elastic to come up:
until $(curl --output /dev/null --silent --fail 'http://localhost/elasticsearch/_cluster/health?wait_for_status=yellow&local=true&timeout=2s');
do
    echo 'waiting for elasticsearch to be YELLOW...'
    sleep 2
done

divider
# wait for minio to come up:
until $(curl --output /dev/null --silent --head --fail http://localhost/minio/health/live);
do
    echo "waiting for minio to come up: checking again in 5 seconds."
    sleep 5
done
echo "Minio is up, things should be good."

divider
# let's se if it can solve something:
echo "submiting job..."
curl --location --request POST 'http://localhost/astrometry/api/agent/submit' \
--header 'Content-Type: application/json' \
--data-raw '{
	"image": "http://nova.astrometry.net/image/8436367",
    "solve_args": {
        "z": 2
    }
}'

divider
# get the list of jobs:
echo "listing running tasks..."
curl -X GET 'http://localhost/astrometry/api/agent/list'
echo

divider
echo "All astrometry services should be booting/running behind traefik.  They're paths should be:"
echo
echo "                Website: http://localhost"
echo "   Astrometry Agent API: http://localhost/astrometry/api"
echo "Minio Storage Dashboard: http://localhost/minio"
echo "                 Kibana: http://localhost/kibana"
echo "      Elasticsearch API: http://localhost/elasticsearch"